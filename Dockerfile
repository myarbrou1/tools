FROM ubi9

ENV OC_VERSION            4.10.53
ENV HELM_VERSION          3.11.2
ENV AZ_VERSION            2.50.0
ENV PY_ANSIBLE_VERSION    7.4.0
ENV PY_KUBERNETES_VERSION 26.1.0
ENV PY_OPENSHIFT_VERSION  0.13.1
ENV PY_OAUTH_VERSION      1.3.1

RUN ln -s /usr/bin/python3 /usr/bin/python && \
    python -m ensurepip --default-pip && \
    pip install ansible==$PY_ANSIBLE_VERSION kubernetes==$PY_KUBERNETES_VERSION openshift==$PY_OPENSHIFT_VERSION requests-oauthlib==$PY_OAUTH_VERSION && \
    \
    curl -o /etc/yum.repos.d/sbt-rpm.repo https://www.scala-sbt.org/sbt-rpm.repo && \
    \
    rpm --import https://packages.microsoft.com/keys/microsoft.asc && \
    dnf install -y https://packages.microsoft.com/config/rhel/9.0/packages-microsoft-prod.rpm && \
    \
    dnf update -y && \
    dnf install -y sbt java-17-openjdk-devel git-core unzip azure-cli-$AZ_VERSION rsync gettext jq bind-utils iputils openldap-clients && \
    dnf clean all && \
    \
    curl -O https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/$OC_VERSION/openshift-client-linux-$OC_VERSION.tar.gz && \
    tar -xvf openshift-client-linux-$OC_VERSION.tar.gz -C /usr/local/bin/ oc kubectl && \
    rm -f openshift-client-linux-$OC_VERSION.tar.gz && \
    chmod 755 /usr/local/bin/oc  && \
    \
    curl -O https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz && \
    tar -xvf helm-v$HELM_VERSION-linux-amd64.tar.gz -C /usr/local/bin/ --strip-components 1 linux-amd64/helm && \
    rm -f helm-v$HELM_VERSION-linux-amd64.tar.gz && \
    chmod 755 /usr/local/bin/helm && \
    \
    curl -L https://github.com/mgdm/htmlq/releases/latest/download/htmlq-x86_64-linux.tar.gz -o htmlq.tar.gz && \
    tar xf htmlq.tar.gz -C /usr/local/bin/ && \
    rm -f htmlq.tar.gz && \
    \
    mkdir /workdir && \
    chgrp -R root /workdir && \
    chmod g+rw -R /workdir

ENV HOME=/workdir

COPY bashrc $HOME/.bashrc

WORKDIR /workdir
