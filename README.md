# tools



## Getting started

oc run -it --rm tools --image=registry.gitlab.com/myarbrou1/tools

oc run -it --rm tools --image=registry.gitlab.com/myarbrou1/tools:edge --image-pull-policy=Always


add to bashrc

`alias tools="oc delete po ocptools > /dev/null 2>&1; oc run -it --rm --image-pull-policy='Always' --env GIT_TOKEN=${GIT_TOKEN} --env VADER_AZ_KEY=${VADER_AZ_KEY} --env DEV_SERVER=\$(oc whoami --show-server)   --env DEV_TOKEN=\$(oc whoami --show-token) --image registry.gitlab.com/myarbrou1/tools:edge ocptools"`  
`alias ptools="podman run -it --rm --pull Always --env GIT_TOKEN=${GIT_TOKEN} --env VADER_AZ_KEY=${VADER_AZ_KEY} --env DEV_SERVER=\$(oc whoami --show-server) --env DEV_TOKEN=\$(oc whoami --show-token) -v $HOME/shared:/workdir/shared:z registry.gitlab.com/myarbrou1/tools:edge"`

run `tools` if logged into openshift or `ptools` locally
