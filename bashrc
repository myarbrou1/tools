PS1='\[\033[38;5;6m\]$?:\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\e[1;31m\h\[\e[0m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ocl="oc login $DEV_SERVER --token $DEV_TOKEN"
alias lo="oc logout"
alias srv="oc whoami --show-server && oc project"
